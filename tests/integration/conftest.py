from unittest import mock

import pytest


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="pop_create_idem")

    with mock.patch("sys.argv", ["pop-create"]):
        hub.pop.config.load(["pop_create"], "pop_create", parse_cli=True)

    yield hub
