---
minimum_pre_commit_version: 2.10.1
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.0.1
    hooks:
      - id: check-merge-conflict
        description: Check for files that contain merge conflict strings.
        language_version: python3
      - id: trailing-whitespace
        description: Trims trailing whitespace.
        args: [--markdown-linebreak-ext=md]
        language_version: python3
      - id: mixed-line-ending
        description: Replaces or checks mixed line ending.
        args: [--fix=lf]
        exclude: make.bat
        language_version: python3
      - id: fix-byte-order-marker
        description: Removes UTF-8 BOM if present, generally a Windows problem.
        language_version: python3
      - id: end-of-file-fixer
        description: Makes sure files end in a newline and only a newline.
        exclude: tests/fake_.*\.key
        language_version: python3
      - id: check-ast
        description: Simply check whether files parse as valid python.
        language_version: python3
        exclude: ({{cookiecutter.root_dir}}/.*)
      - id: check-yaml
      - id: check-json

  # ----- Formatting ---------------------------------------------------------------------------->
  - repo: https://github.com/saltstack/pre-commit-remove-import-headers
    rev: 1.1.0
    hooks:
      - id: remove-import-headers

  - repo: https://github.com/asottile/pyupgrade
    rev: v2.18.2
    hooks:
      - id: pyupgrade
        name: Rewrite Code to be Py3.6+
        args: [--py36-plus]

  - repo: https://github.com/asottile/reorder_python_imports
    rev: v2.5.0
    hooks:
      - id: reorder-python-imports
        args: [--py36-plus]
        exclude: ({{cookiecutter.root_dir}}/.*)

  - repo: https://github.com/psf/black
    rev: 20.8b1
    hooks:
      - id: black
        args: [--target-version=py38]
        exclude: ({{cookiecutter.root_dir}}/.*)

#  - repo: https://github.com/asottile/blacken-docs
#    rev: v1.10.0
#    hooks:
#      - id: blacken-docs
#        additional_dependencies: [black==20.8b1]
#        exclude: docs/source/tutorial/migrate_salt_cloud.rst
  # <---- Formatting -----------------------------------------------------------------------------

  # ----- Static Requirements ------------------------------------------------------------------->
  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: '3.0'
    hooks:
      - id: pip-tools-compile
        alias: compile-3.6-test-requirements
        name: Py3.6 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.6
          - --platform=linux
          - requirements/tests.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: '3.0'
    hooks:
      - id: pip-tools-compile
        alias: compile-3.7-test-requirements
        name: Py3.7 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.7
          - --platform=linux
          - requirements/tests.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: '3.0'
    hooks:
      - id: pip-tools-compile
        alias: compile-3.8-test-requirements
        name: Py3.8 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.8
          - --platform=linux
          - requirements/tests.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: '3.0'
    hooks:
      - id: pip-tools-compile
        alias: compile-3.9-test-requirements
        name: Py3.9 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.9
          - --platform=linux
          - requirements/tests.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: '3.0'
    hooks:
      - id: pip-tools-compile
        alias: compile-3.10-test-requirements
        name: Py3.10 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.10
          - --platform=linux
          - requirements/tests.in

  # <---- Static Requirements --------------------------------------------------------------------
