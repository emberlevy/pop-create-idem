PoP-isms
========

\__func_alias__
---------------

.. code-block:: python


    # __func_alias__ makes sure the "list_" function is put on the hub as "list"
    # We use this for function names that clash with python builtin names
    __func_alias__ = {"list_": "list"}

    # If this function is in the file project_root/project_name/exec/my_cloud/instance.py,
    # It can be called from code like this: hub.exec.my_cloud.instance.list(ctx)
    async def list_(hub, ctx):
        ...
