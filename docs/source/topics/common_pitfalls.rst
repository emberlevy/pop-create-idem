Notes
=====

Please, please, please use async libraries as much possible when implementing clouds in idem.
Making HTTP requests is the throttle point of this application.
Using something like aiohttp_ under the hood for your api requests means other idem clouds won't be set back waiting
for your cloud's operations to complete when running multi-cloud sls files.

.. _aiohttp: https://docs.aiohttp.org/en/stable/
